# Fork's README

This is a fork of [CitadelApp/cordova-plugin-file-opener2](https://github.com/CitadelApp/cordova-plugin-file-opener2). 
The modifications are
- the renaming of the variable pluginResult by pluginResultFileOpener which prevented the compilation on ios,
- delete the possibility to install/uninstall an apk with this plugin (source : [https://github.com/pwlin/cordova-plugin-file-opener2/issues/329#issuecomment-1194040883](https://github.com/pwlin/cordova-plugin-file-opener2/issues/329#issuecomment-1194040883))
- delete the option "uninstall" and "appIsInstalled" on Android

[CitadelApp/cordova-plugin-file-opener2](https://github.com/CitadelApp/cordova-plugin-file-opener2) is also a fork of [pwlin/cordova-plugin-file-opener2](https://github.com/pwlin/cordova-plugin-file-opener2) version `3.0.5`.
The only modification is the removal of hardcoded permission `android.permission.REQUEST_INSTALL_PACKAGES`.
